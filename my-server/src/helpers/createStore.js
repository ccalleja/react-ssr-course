import {createStore, applyMiddleware} from "redux";
import thunk from "redux-thunk";
import reducers from "../client/reducers";
import axios from "axios/index";

export default (req) => {
    //create a custom axios configration
    const axiosInstace = axios.create({
        baseURL: "https://react-ssr-api.herokuapp.com/",
        headers: {
            cookie: req.get("cookie") || '' //default to blank string not undefined
        }
    });

    const store = createStore(
        reducers,
        {},
        applyMiddleware(thunk.withExtraArgument(axiosInstace))
    );

    return store;
}