import {FETCH_CURRENT_USER} from "../actions";

export default (state = null, action) => {
    switch (action.type) {
        case FETCH_CURRENT_USER: {
            // if user is logged out we receive no data
            console.log("User auth state:", action.payload.data);
            return action.payload.data || false;
        }
        default:
            return state;
    }
}