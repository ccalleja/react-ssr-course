import React, {Component} from "react";
import {connect} from "react-redux";
import {fetchAdmins} from "../actions/index";
import requireAuth from "../components/hocs/requireAuth";

class AdminsListPage extends Component {
    componentDidMount(){
        if(!this.props.admins || this.props.admins.length === 0)
            this.props.fetchAdmins();
    }

    renderAdmins(){
        return this.props.admins.map(admin => {
            return <li key={admin.id}>{admin.name}</li>;
        });
    }

    render(){
        return (
            <div>
                <h3>Protected list of Admins:</h3>
                <ul>{this.renderAdmins()}</ul>
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        admins: state.admins
    }
}

/**
 * Function used on server for ssr
 * It loads all data and sets it in the store
 *
 * @param store: the store generated server side
 */
function loadData(store){
    return store.dispatch(fetchAdmins());
}

export {loadData};
export default {
    component: connect(mapStateToProps, {fetchAdmins})(
        requireAuth(AdminsListPage)
    ),
    loadData
};