import React from "react";

/**
 * @param staticContext: is only passed using static router so we default to {}
 * @returns {*}
 * @constructor
 */
const NotFoundPage = ({staticContext = {}}) => {
    staticContext.notFound = true;
    return <h1>Ooops, route not found!</h1>;
}

export default {
   component: NotFoundPage
}