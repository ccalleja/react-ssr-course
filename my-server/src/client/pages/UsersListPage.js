import React, {Component} from "react";
import {connect} from "react-redux";
import {fetchUsers} from "../actions/index";
import {Helmet} from "react-helmet";

class UsersList extends Component {
    componentDidMount(){
        if(!this.props.users || this.props.users.length === 0)
            this.props.fetchUsers();
    }

    renderUsers(){
        return this.props.users.map(user => {
            return <li key={user.id}>{user.name}</li>;
        });
    }

    head(){
        return (
            <Helmet>
                <title>{`${this.props.users.length} Users Loaded`}</title>
                <meta property="og:title" content="Users App"/>
            </Helmet>
        );
    }
    render(){
        return (
            <div>
                {this.head()}
                Here is a list of users:
                <ul>{this.renderUsers()}</ul>
            </div>
        );
    }
}

function mapStateToProps(state){
    return {
        users: state.users
    }
}

/**
 * Function used on server for ssr
 * It loads all data and sets it in the store
 *
 * @param store: the store generated server side
 */
function loadData(store){
    return store.dispatch(fetchUsers());
}

export {loadData};
export default {
    component: connect(mapStateToProps, {
        fetchUsers
    })(UsersList),
    loadData
};