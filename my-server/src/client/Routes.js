import React from "react";
import HomePage from "./pages/HomePage";
import UsersListPage from "./pages/UsersListPage";
import App from "./App";
import NotFoundPage from "./pages/NotFoundPage";
import AdminsListPage from "./pages/AdminsListPage";

/*
Replaced to deal with SSR data loading using component descriptor functions
export default () => {
    return (
        <div>
            <Route exact path="/" component={Home} />
            <Route path="/users" component={UsersList} />
        </div>
    );
}
*/

export default [
    {
        ...App,
        routes: [
            {
                ...HomePage,
                // component: HomePage, //the above is es6 suyntax instead of this
                path: "/",
                exact: true
            },
            {
                ...UsersListPage,
                /*
                the aboive is es6 suyntax instead of the below
                component: UsersListPage,
                loadData
                */
                path: "/users"
            },
            {
                ...AdminsListPage,
                path: "/admins"
            },
            {
                ...NotFoundPage
            }

        ]
    }
];