// Client side index file
import 'babel-polyfill';
import React from "react";
import ReactDom from "react-dom";
import Routes from "./Routes";
import {BrowserRouter} from "react-router-dom";
import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import reducers from "./reducers";
import {renderRoutes} from "react-router-config";
import axios from "axios";

//create a custom axios configration
const axiosInstace = axios.create({
    baseURL: "/api"
});

const store = createStore(
    reducers,
    window.INITIAL_STATE,
    applyMiddleware(thunk.withExtraArgument(axiosInstace))
);

// ReactDom.render( changed to hydrate, optimised for ssr
ReactDom.hydrate(
    <Provider store={store}>
        <BrowserRouter>
            {/*Replaced for SSR data loading <Routes/>*/}
            <div>{renderRoutes(Routes)}</div>
        </BrowserRouter>
    </Provider>,
    document.querySelector("#root")
);

console.log("Client JS loaded, hydration done!!")