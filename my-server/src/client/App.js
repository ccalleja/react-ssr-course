import React from "react";
import {renderRoutes} from "react-router-config";
import Header from "./components/Header";
import {fetchCurrentUser} from "./actions";

//the route is passed via the react-router-config
const App = ({route}) => {
    return (
        <div>
            <Header />
            {renderRoutes(route.routes)}
        </div>
    );
};

export default {
    component: App,
    loadData: ({ dispatch }) => {
        return dispatch(fetchCurrentUser())
    }
};