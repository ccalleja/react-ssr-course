const path = require('path');
const merge = require('webpack-merge');
const baseConfig = require('./webpack.base.js');
const webpackNodeExternals = require("webpack-node-externals");

const config = module.exports = {
    // inform webpack that we re building a bundle for nodejs not browser
    target: 'node',

    // root file for our server application
    entry: './src/index.js',

    // where to put the output file
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'build')
    },

    // server side, do not include the node modules in the bundle
    externals: [webpackNodeExternals()]
};


module.exports = merge(baseConfig, config);
